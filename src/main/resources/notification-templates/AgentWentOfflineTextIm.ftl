[#-- @ftlvariable name="agent" type="com.atlassianlab.bamboo.plugins.agentnotification.Agent" --]
[#-- @ftlvariable name="baseUrl" type="java.lang.String" --]
${i18n.getText("agent.notification.offline.title", [agent.name?html])}
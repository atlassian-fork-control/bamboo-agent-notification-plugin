
[#-- @ftlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" --]
[#-- @ftlvariable name="buildsPager" type="com.atlassian.bamboo.filter.Pager<BuildResultsSummary>" --]
[#-- @ftlvariable name="agent" type="com.atlassianlab.bamboo.plugins.agentnotification.Agent" --]
[#-- @ftlvariable name="baseUrl" type="java.lang.String" --]
[#include "templateHtml.ftl"]
[@templateOuter baseUrl=baseUrl]
<div id="title-status" class="unknown">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td id="title-status-icon">
                <img src="${baseUrl}/images/iconsv4/icon-build-failed-w.png" alt="Agent offline">
            </td>
            <td id="title-status-text">
            ${i18n.getText("agent.notification.offline.text")} <strong>${agent.name?html}</strong>
            </td>
        </tr>
        </tbody>
    </table>
</div>
    [#if buildsPager.totalSize > 0]
    <h2>${i18n.getText("agent.notification.offline.recent.builds")}</h2>
    <br />
    <table id="buildResultsTable" width="100%" cellpadding="0" cellspacing="0" class="aui" >
        <thead>
        <tr>
            <th>${i18n.getText("buildResult.completedBuilds.buildNumber")}</th>
            <th>${i18n.getText("buildResult.completedBuilds.completionTime")}</th>
            <th>${i18n.getText("buildResult.completedBuilds.duration")}</th>
        </tr>
        </thead>
        <tbody>
            [#list (buildsPager.page.list)! as buildResult]
            <tr>
                <td><img src="${getIcon(baseUrl, buildResult.buildState)}" alt="${buildResult.buildState.name()?html}">
                    [#assign plan=buildResult.immutablePlan]
                    <a href="${baseUrl}/browse/${plan.project.key}">${plan.project.name?html}</a> &rsaquo;
                    <a href="${baseUrl}/browse/${plan.parent.key}">${plan.parent.buildName?html}</a> &rsaquo;
                    [#assign planResultKey=buildResult.planResultKey /]
                    <a href="${baseUrl}/browse/${plan.parent.key}-${planResultKey.buildNumber}">#${planResultKey.buildNumber}</a> &rsaquo;
                    <a href="${baseUrl}/browse/${planResultKey}">${plan.buildName?html}</a>
                </td>

                [#if buildResult.buildCompletedDate?has_content ]
                    [#assign buildDate=buildResult.buildCompletedDate?datetime?string("EEE, d MMM, hh:mm a")]
                [#else]
                    [#assign buildDate]
                        ${i18n.getText("buildResult.completedBuilds.defaultDurationDescription")}
                    [/#assign]
                [/#if]

                <td>${buildDate}</td>
                <td>${buildResult.processingDurationDescription}</td>
            </tr>
            [/#list]
        </tbody>
    </table>
    [/#if]

    [@showActions]
        <span class="email-list-item"><a href="${baseUrl}/agent/viewAgent.action?agentId=${agent.id}">View agent</a></span>
    [/@showActions]
[/@templateOuter]

[#function getIcon baseUrl buildState=""]
[#-- @ftlvariable name="baseUrl" type="java.lang.String" --]
[#-- @ftlvariable name="buildState" type="com.atlassian.bamboo.builder.BuildState" --]
    [#if buildState.name() == 'UNKNOWN']
        [#return "${baseUrl}/images/iconsv4/icon-build-unknown.png"]
    [#elseif buildState.name() == 'SUCCESS']
        [#return "${baseUrl}/images/iconsv4/icon-build-successful.png"]
    [#elseif buildState.name() == 'FAILED']
        [#return "${baseUrl}/images/iconsv4/icon-build-failed.png"]
    [#else]
        [#return "${baseUrl}/images/iconsv4/icon-build-unknown.png"]
    [/#if]
[/#function]
[#include "notificationCommonsHtml.ftl"][#lt/]
[#macro templateOuter baseUrl]
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <style type="text/css">
        a:hover, a:focus, input.postlink:hover, input.postlink:focus { text-decoration: underline !important;}
        a.button:focus, a.button:hover {
            text-decoration: none !important;
        }
        @media handheld, only screen and (max-device-width: 480px) {
            div, a, p, td, th, li, dt, dd {
                -webkit-text-size-adjust: auto;
            }
            small, small a {
                -webkit-text-size-adjust: 90%;
            }
            small[class=email-metadata] {
                -webkit-text-size-adjust: 93%;
                font-size: 12px;
            }

            table[id=email-wrap] > tbody > tr > td {
                padding: 2px !important;
            }
            table[id=email-wrap-inner] > tbody > tr > td {
                padding: 8px !important;
            }
            table[id=email-footer] td {
                padding: 8px 12px !important;
            }
            table[id=email-actions] td {
                padding-top: 0 !important;
            }
            table[id=email-actions] td.right {
                text-align: right !important;
            }
            table[id=email-actions] .email-list-item {
                display: block;
                margin: 1em 0 !important;
                word-wrap: normal !important;
            }
            span[class=email-list-divider] {
                display: none;
            }
        }
    </style>
</head>
<body>
<table id="email-wrap" align="center" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td>
            <table id="email-wrap-inner" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td>
                        [#nested]
                    </td>
                </tr>
                </tbody>
            </table>
            [@showEmailFooter baseUrl=baseUrl /]
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
[/#macro]
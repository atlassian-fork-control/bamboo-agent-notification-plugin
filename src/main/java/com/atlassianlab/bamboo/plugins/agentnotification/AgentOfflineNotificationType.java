package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.agent.AgentType;
import com.atlassian.bamboo.notification.AbstractNotificationType;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.struts.TextProvider;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

public class AgentOfflineNotificationType extends AbstractNotificationType {

    private final TemplateRenderer templateRenderer;
    private final TextProvider textProvider;

    @Inject
    public AgentOfflineNotificationType(@ComponentImport TemplateRenderer templateRenderer,
                                        @ComponentImport TextProvider textProvider) {
        super();
        this.templateRenderer = templateRenderer;
        this.textProvider = textProvider;
    }

    @Override
    public boolean isNotificationRequired(@NotNull Object event) {
        final AgentOfflineEvent offlineEvent = Narrow.downTo(event, AgentOfflineEvent.class);
        return offlineEvent != null && offlineEvent.getBuildAgent().getType() != AgentType.ELASTIC;
    }

    @NotNull
    @Override
    public String getEditHtml() {
        final String editTemplateLocation = notificationTypeModuleDescriptor.getEditTemplate();
        Map<String, Object> context = new HashMap<>();
        return StringUtils.defaultString(templateRenderer.render(editTemplateLocation, context));
    }

    @NotNull
    public String getViewHtml() {
        return textProvider.getText("agent.notification.offline.table.view.remote");
    }

    @NotNull
    @Override
    public String getConfigurationData() {
        return "";
    }
}

package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.buildqueue.PipelineDefinition;
import com.atlassian.bamboo.event.agent.AgentRegisteredEvent;
import com.atlassian.bamboo.template.TemplateRenderer;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AgentOnlineNotificationTest {
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private TemplateRenderer templateRenderer;
    @InjectMocks private AgentOnlineNotification notification;

    @Test
    public void agentIsAddedForTemplates() {
        final PipelineDefinition pipelineDefinition = mock(PipelineDefinition.class);
        notification.setEvent(new AgentRegisteredEvent(this, pipelineDefinition));

        notification.getHtmlEmailContent();
        verify(templateRenderer).render(anyString(), argThat(argument -> argument.containsKey(AgentOnlineNotification.CFG_AGENT)));
    }

}
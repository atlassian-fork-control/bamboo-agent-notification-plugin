package com.atlassianlab.bamboo.plugins.agentnotification;

import com.atlassian.bamboo.buildqueue.PipelineDefinition;
import com.atlassian.bamboo.event.agent.AgentRegisteredEvent;
import com.atlassian.bamboo.notification.NotificationDispatcher;
import com.atlassian.bamboo.notification.NotificationManager;
import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.notification.NotificationRuleImpl;
import com.atlassian.bamboo.notification.NotificationType;
import com.atlassian.bamboo.notification.SystemNotificationService;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.events.AgentOfflineEvent;
import com.atlassian.struts.TextProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AgentNotificationListenerTest {
    private static final String RECIPIENT_KEY = "recipientKey";
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock private SystemNotificationService systemNotificationService;
    @Mock private NotificationDispatcher notificationDispatcher;
    @Mock private NotificationManager notificationManager;
    @Mock private TemplateRenderer templateRenderer;
    @Mock private TextProvider textProvider;
    private AgentNotificationListener listener;

    @Before
    public void setUp() {
        listener = new AgentNotificationListener(systemNotificationService, notificationManager, notificationDispatcher) {
            @NotNull
            @Override
            protected <T extends AgentChangeStatusNotification> T createNotification(Class<T> notificationClass) {
                try {
                    return notificationClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    return null;
                }
            }
        };
        when(notificationManager.getNotificationRecipientFromKey(RECIPIENT_KEY)).thenReturn(mock(NotificationRecipient.class));
        when(notificationManager.getNotificationTypeFromKey(Keys.OFFLINE_NOTIFICATION_PLUGIN_KEY)).thenReturn(new AgentOfflineNotificationType(templateRenderer, textProvider));
        when(notificationManager.getNotificationTypeFromKey(Keys.ONLINE_NOTIFICATION_PLUGIN_KEY)).thenReturn(new AgentOnlineNotificationType(templateRenderer, textProvider));
    }

    @Test
    public void agentOfflineEventSendsNotification() {
        createNotificationRule(Keys.OFFLINE_NOTIFICATION_PLUGIN_KEY);

        listener.onAgentOfflineHandler(new AgentOfflineEvent(this, mock(BuildAgent.class)));

        verify(notificationDispatcher).dispatchNotifications(any(AgentOfflineNotification.class));
    }

    @Test
    public void agentRegisteredEventSendsNotification() {
        createNotificationRule(Keys.ONLINE_NOTIFICATION_PLUGIN_KEY);

        listener.onAgentOnlineHandler(new AgentRegisteredEvent(this, mock(PipelineDefinition.class)));
        verify(notificationDispatcher).dispatchNotifications(any(AgentOnlineNotification.class));
    }

    private void createNotificationRule(String conditionKey) {
        final NotificationRuleImpl notificationRule = new NotificationRuleImpl();
        notificationRule.setConditionKey(conditionKey);
        notificationRule.setRecipientType(RECIPIENT_KEY);
        notificationRule.setNotificationManager(notificationManager);
        List<NotificationRule> notificationRules = Collections.singletonList(notificationRule);
        when(systemNotificationService.getSystemNotificationRules()).thenReturn(notificationRules);
        final NotificationType notificationType = mock(NotificationType.class);
        when(notificationType.isNotificationRequired(any())).thenReturn(true); //we want to send notifications always
        when(notificationManager.getNotificationType(eq(notificationRule))).thenReturn(notificationType);

        when(notificationManager.getNotificationRecipient(eq(notificationRule))).thenReturn(mock(NotificationRecipient.class));
    }
}
package com.atlassianlab.bamboo.plugins.agentnotification.pageobject;

import com.atlassian.bamboo.pageobjects.pages.AbstractBambooPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.notification.ConfigureNotificationComponent;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class SystemNotificationsPage extends AbstractBambooPage {
    @ElementBy(id = "addSystemNotification")
    private PageElement addNotificationBtn;

    @ElementBy(id = "systemNotificationForm")
    private PageElement addNotificationForm;

    @Override
    public PageElement indicator() {
        return addNotificationBtn;
    }

    @Override
    public String getUrl() {
        return "/admin/viewSystemNotifications.action";
    }

    public ConfigureNotificationComponent addNotification() {
        this.addNotificationBtn.click();
        Poller.waitUntilTrue(this.addNotificationForm.timed().isVisible());
        return this.pageBinder.bind(ConfigureNotificationComponent.class);
    }
}

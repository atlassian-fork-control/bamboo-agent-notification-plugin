package com.atlassianlab.bamboo.plugins.agentnotification.pageobject;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.inject.Inject;
import org.openqa.selenium.By;

/**
 * Extends Bamboo's page as it doesn't contain logic to disable remote agent authentication
 */
public class AgentsConfigurationPage extends com.atlassian.bamboo.pageobjects.pages.admin.agent.AgentsConfigurationPage {
    @ElementBy(id = "disableRemoteAgentAuthentication")
    private PageElement disableRemoteAgentAuthentication;

    @Inject
    private PageElementFinder finder;

    public void disableAgentAuthentication() {
        if (disableRemoteAgentAuthentication.isPresent()) {
            disableRemoteAgentAuthentication.click();
            finder.find(By.id("confirmAgentAuthenticationForm")).find(By.name("save")).click();
        }
    }
}
